using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    [SerializeField] private float speed = 0.01f;//f specifies its a float
    
    
    private void Awake()
    {
        //when object is created
        Debug.Log("This is a debug statement");
    }

    // Start is called before the first frame update
    private void Start()
    {
        Debug.Log("This is a Start Function");

        Vector3 newPosition = new Vector3(-5f, 1f, 0f);
        transform.position = newPosition;
        //GameObject gameObjectThisScriptIsOn = gameObject;
        //transform gets this gameobject and transforms it 
        
    }

    // Update is called once per frame
    private void Update()
    {
    //find new x position
        float newXPos = transform.position.x + speed;
        //vector that represents whole new position
        Vector3 newPosition = new Vector3(newXPos, transform.position.y, transform.position.z);
        //tell transform to set new position 
        transform.position = newPosition;
        
    }
    private void FixedUpdate(){ //runs once every time physics is updated 
    //50 times every second
        
        
        
    }

}
