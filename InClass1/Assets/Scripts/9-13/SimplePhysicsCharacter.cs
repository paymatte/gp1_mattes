using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float accelSpeed = 10f;
    private void Awake(){
    }
    // Start is called before the first frame update
    void Start()
    {
        Vector2 force = new Vector2(20f,0f);
        //Vector2 is just two floats (x,y)
        //vectors can represent both position and a direction
        //point (-1, -1) + a 
        //a is (0.5, 0.5)
        //(-0.5, -0.5)
        
        
        rb.AddForce(force, ForceMode2D.Impulse);

    }

    void FixedUpdate(){
        //make sure Rigidbody2D isnt null

        //Force = Mass x Acceleration
        //we are giving it how much we want it to Accelerate
        //-10f in x means that we are Accelerating at 10m/s to the left
        if(rb != null){
        //-1f = -1f*10f*Time.fixedDeltaTime

        //MovePosition with Rigidbody2D is only for kenimatic rigidbodies (goes as fast as posible)
            Vector2 forceOverTime = new Vector2(-1f * accelSpeed * Time.fixedDeltaTime, 0f);

            rb.AddForce(forceOverTime, ForceMode2D.Force);

            //Time.Time (time in seconds since the start of the game)
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
