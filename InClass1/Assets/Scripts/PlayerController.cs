using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{

    private InputActions playerInputActions;

    private void Awake()
    {
        playerInputActions = new InputActions();
    }


    // Start is called before the first frame update

    private void OnEnable()
    {
        //Binding our inputs action functions 
        playerInputActions.Player.Move.started += MoveAction;
        playerInputActions.Player.Move.performed += MoveAction;
        playerInputActions.Player.Move.canceled += MoveAction;

        playerInputActions.Player.Dance.performed += DanceActionPerformed;

        //enable player action map to listen for our imnput
        playerInputActions.Player.Enable();
    }

    private void MoveAction(InputAction.CallbackContext context)
    {
        Vector2 movementInput = context.ReadValue<Vector2>();
        Debug.Log("move " + movementInput);
    }
    private void DanceActionPerformed(InputAction.CallbackContext context)
    {
        Debug.Log("tryingf tp dance");
    }

    private void OnDisable()
    {
        playerInputActions.Player.Move.started -= MoveAction;
        playerInputActions.Player.Move.performed -= MoveAction;
        playerInputActions.Player.Move.canceled -= MoveAction;

        playerInputActions.Player.Dance.performed -= DanceActionPerformed;

        //enable player action map to listen for our imnput
        playerInputActions.Player.Disable();
    }
 

    // Update is called once per frame
    void Update()
    {
        
    }
}
