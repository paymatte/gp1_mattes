using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private float speedT = 0.01f;
    [SerializeField] private Vector3 pointOne = Vector3.zero;
    [SerializeField] private Vector3 pointTwo = new Vector3(-5f, 5f, 0f);
    [SerializeField] private bool isOne = true;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(Vector3.Distance(transform.position, pointOne) < 0.001f)
        {
            isOne = false;
            Debug.Log("false");
        }else if(Vector3.Distance(transform.position, pointTwo) < 0.001f)
        {
            isOne=true;
            Debug.Log("true");
        }

        if (isOne)
        {
            transform.position = Vector3.MoveTowards(transform.position, pointOne, speedT);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, pointTwo, speedT);
        }
    }

}
