using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ForceMovement : MonoBehaviour
{

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float accelSpeed = 10f;
    [SerializeField] private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        accelSpeed = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {

        //Vector3 movement = transform.position;

        if (rb != null)
        {

            Vector2 forceOverTime = Vector2.zero;
            Keyboard keyboard = Keyboard.current;
            if (keyboard != null && keyboard.dKey.IsPressed())
            {
                forceOverTime = new Vector2( accelSpeed * Time.fixedDeltaTime, 0f);
                spriteRenderer.flipX = false;
            }
            if (keyboard != null && keyboard.aKey.IsPressed())
            {
                forceOverTime = new Vector2(-1f * accelSpeed * Time.fixedDeltaTime, 0f);
                spriteRenderer.flipX = true;

            }
            if (keyboard != null && keyboard.wKey.IsPressed())
            {
                forceOverTime = new Vector2(forceOverTime.x, accelSpeed * Time.fixedDeltaTime);

            }

            Debug.Log(forceOverTime);

            rb.AddForce(forceOverTime, ForceMode2D.Impulse);


        }
    }
}
