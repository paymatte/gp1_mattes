using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSprite : MonoBehaviour
{

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Vector2 pointOne = new Vector2(5f, 0f);
    [SerializeField] private Vector2 pointTwo = new Vector2(0f, 0f);
    [SerializeField] private Vector2 speed = new Vector2(5f, 0);
    private bool goToOne = true;
    // Start is called before the first frame update
    void Start()
    {
        pointOne = new Vector2(5f, 0f);
        pointTwo = new Vector2(0f, 0f);
        speed = new Vector2(5f, 0);
        rb.position = pointOne;


    }

    // Update is called once per frame
    void Update()
    {
       // Debug.Log("speed " + speed);
       // Debug.Log("point " + transform.position);
    }

    private void FixedUpdate()
    {
        if (rb != null)
        {
            if (Vector2.Distance(rb.position, pointOne) < 0.05f) {

                goToOne = false;
            }
            else if(Vector2.Distance(rb.position, pointTwo) < 0.05f)
            {
                goToOne = true;

            }

            if(goToOne)
            {
                rb.MovePosition(rb.position + speed*Time.fixedDeltaTime);

            }
            else
            {
                rb.MovePosition(rb.position + -1 * speed * Time.fixedDeltaTime);
            }
        }
    }
}
